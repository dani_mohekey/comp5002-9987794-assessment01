﻿using System;

namespace comp5002_9987794_assessment01
{
    class Program
    {
        static void Main(string[] args)
        {
            //Start the program with Clear();
            Console.Clear();
            
            // Display welcome message on screen
            Console.WriteLine("Hello! Welcome to Danis store");


                // Declare variables
                string name = "";
                var total = 0.0;
                var addNewNumber = false;


            // Ask user to enter name
            Console.WriteLine("");
            Console.WriteLine("Please enter your name.");
            name = Console.ReadLine();
            Console.WriteLine("");


            // Ask user to enter two decimal number
            Console.WriteLine("");
            Console.WriteLine("Please enter a number with two decimal places");
            total = double.Parse(Console.ReadLine());
            Console.WriteLine("");


            // Ask user if they want to add another number
            Console.WriteLine("");
            Console.WriteLine("Would you like to add another number?");
            Console.WriteLine("true = Yes | false = No");
            Console.WriteLine("");


            addNewNumber = bool.Parse(Console.ReadLine());

 
                if (addNewNumber) // If yes
                {
                    Console.WriteLine("");
                    Console.WriteLine("Please enter another number with two decimal places."); //Ask user to enter second number.
                    total += double.Parse(Console.ReadLine());
                    Console.WriteLine("");
                }

            
            // Display total Pre-GTS
            Console.WriteLine("");
            Console.WriteLine($"Pre-GST Total: {total} ");
            Console.WriteLine("");


            // Display total including GST
            Console.WriteLine("");
            Console.WriteLine($"Total Including GST {(total * 0.15) + total} ");
            Console.WriteLine("");

            
            // Goodbye message, thank user for shopping at store
            Console.WriteLine("");
            Console.WriteLine("Thank you for shopping at Danis store. Goodbye");
            Console.WriteLine("");


            //End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();
        }
    }
}
